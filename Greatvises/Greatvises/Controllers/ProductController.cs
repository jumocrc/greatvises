﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Greatvises.Models;

namespace Greatvises.Controllers
{
    public class ProductController : ApiController
    {
       public static LinkedList<Category> shoes = new LinkedList<Category>();
       public static LinkedList<Category> foods = new LinkedList<Category>();
       public static LinkedList<Category> tools = new LinkedList<Category>();

      
        Category shoe1 = new Category { Id = 1, Name = "Shoes" };    
        Category food1 = new Category { Id = 2, Name = "Food" };
        Category tool1 = new Category { Id = 3, Name = "Tools" };

        Product[] products = new Product[]
        {
            new Product { Id = 1, Name = "Tomato Soup", Categories = foods, Price = 2500, Description = "Sopa de tomate para pan", Quantity = 100 },
            new Product { Id = 2, Name = "Shoes for baby", Categories = shoes, Price = 8899, Description = "Zapatos de muer para bebe", Quantity = 500},
            new Product { Id = 3, Name = "Hammer", Categories = tools, Price = 10000, Description = "Martillo de acero inoxidable", Quantity = 2500}
        };

        public void fillAll()
        {
            shoes.AddFirst(shoe1);
            foods.AddFirst(food1);
            tools.AddFirst(tool1);
        }

        public IEnumerable<Product> GetAllProducts()
        {
            fillAll();
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            fillAll();
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);      
    }
}
}
