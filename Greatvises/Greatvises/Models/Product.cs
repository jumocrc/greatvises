﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Greatvises.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public LinkedList<Category> Categories { get; set; }
        public string Description { get; set;}
        public decimal Price { get; set; }
        public float Quantity { get; set; }

    }
}